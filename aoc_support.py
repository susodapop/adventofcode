import unittest


def bring_input(filename):
    with open(filename, "r") as f:
        content = f.read()
        return content


class TestBasicBehavior(unittest.TestCase):

    def test_standard_behavior(self):
        x = bring_input("input.txt")
        self.assertEqual(x, 'Hello World!')
