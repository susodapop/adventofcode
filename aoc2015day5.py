from aoc_support import bring_input
import unittest
import re

def naughty_or_nice(m_input):

    # Vowel counter
    vowels = {'a': 0, 
              'e': 0,
              'i': 0,
              'o': 0,
              'u': 0}

    double_letter = re.compile(r"(.)\1")
    forbidden_combos = re.compile(r"(ab|cd|pq|xy)")

    #Vowel counting
    for letter in m_input:
        if letter in vowels.keys():
            vowels[letter] += 1

    if sum(vowels.values()) < 3:
        return 'naughty'

    #Double letter checking

    if re.search(double_letter, m_input) == None:
        return 'naughty'

    # forbidden sequence checks

    if re.search(forbidden_combos, m_input) != None:
        return 'naughty'

    return 'nice'

def naughty_or_nice_two(m_input):


    letter_pair = re.compile(r"(..).*\1")
    triplet = re.compile(r"(.).\1")

    #pair of double letters not overlapping

    if re.search(letter_pair, m_input) == None:
        return 'naughty'

    # check for xyx triplets

    if re.search(triplet, m_input) == None:
        return 'naughty'

    return 'nice'


m_input = bring_input('2015d5.txt')
list_m_input = m_input.split('\n')
counter1 = 0
counter2 = 0

for i in list_m_input:
    if naughty_or_nice(i) == 'nice':
        counter1 += 1

for i in list_m_input:
    if naughty_or_nice_two(i) == 'nice':
        counter2 += 1

print('There were {} nice strings'.format(counter1))
print('There were {} nice strings the second time around'.format(counter2))

class TestBasicBehaviorPartOne(unittest.TestCase):

    def test_first_example(self):
        self.assertEqual('nice', naughty_or_nice('ugknbfddgicrmopn'))

    def test_second_example(self):
        self.assertEqual('nice', naughty_or_nice('aaa'))

    def test_third_example(self):
        self.assertEqual('naughty', naughty_or_nice('jchzalrnumimnmhp'))

    def test_fourth_example(self):
        self.assertEqual('naughty', naughty_or_nice('haegwjzuvuyypxyu'))

    def test_fifth_example(self):
        self.assertEqual('naughty', naughty_or_nice('dvszwmarrgswjxmb'))

class TestBasicBehaviorPartTwo(unittest.TestCase):

    def test_first_example(self):
        self.assertEqual('nice', naughty_or_nice_two('qjhvhtzxzqqjkmpb'))

    def test_second_example(self):
        self.assertEqual('nice', naughty_or_nice_two('xxyxx'))

    def test_third_example(self):
        self.assertEqual('naughty', naughty_or_nice_two('uurcxstgmygtbstg'))

    def test_fourth_example(self):
        self.assertEqual('naughty', naughty_or_nice_two('ieodomkazucvgmuy'))