import aoc_support
import unittest
from itertools import count
from hashlib import md5    

def secret_key_gen(key='bgvyzdsv', len=5):

    for i in count(1):
        pattern = ['0' for i in range(len)]
        pattern = ''.join(pattern)
        input = "{}{}".format(key,i)
        h = md5(input.encode('utf-8')).hexdigest()[:len]
        if h == pattern:
            return "{}".format(i)
            break

print("The first solution is {}".format(secret_key_gen()))
print("The second solution is {}".format(secret_key_gen(len=6)))


class TestBasicBehavior(unittest.TestCase):

    def test_first_example(self):
        secret_key = secret_key_gen('abcdef')
        self.assertEqual(secret_key, '609043')

    def test_second_example(self):
        secret_key = secret_key_gen('pqrstuv')
        self.assertEqual(secret_key, '1048970')